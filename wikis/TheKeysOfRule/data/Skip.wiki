﻿++ Skip

Meštar guilda [Guild Alea|Alea]. Stari halfling (oko 150 godina, efektivno 105 zahvaljujući druidizmu) sijede proćelave kose, i naravi koja se kosi s njegovom dobi (osim u epizodama križobolje).

Skip je osoba bez prezimena. Odrastao je kao siroče građanskog rata, na prljavim ulicama grada [Taurel|Taurela]. Taurel, u to doba pod vilenjačkom vlašću, pati zbog ratnih razaranja i visokih poreza nametnutih radi financiranja daljnjih osvajanja. Skip je dobio svoje ime zbog svoje okretnosti i spretnosti, te zahvaljujući tome i njegovom brzom umu uspješno preživljava gdje mnoga djeca ne uspijevaju. No umjesto da se brine samo za sebe, on vodi malu bandu klinaca. Zajedno si uspiju priskrbljivati dovoljno da ne umiru od gladi, i jedni drugima predstavljaju svojevrsnu obitelj.

Kada izraste do otrpilike 10 godina, grad dođe pod napad jedne radikalne ljudske skupine. Plamen uništi većinu grada, a ljudi su opljačkali ono malo vrijednih stvari što je grad imao. Skip je u šoku gledao kako njegovi prijatelji umiru u vatri ili pod mačem razbojnika. Obuzme ga veliki strah i on pobježe u šumu, ostavivši sve iza sebe.

U šumi je neko vrijeme živio sam, jedva uspijevajući preživljavati zahvaljujući vještinama zarađenim u gradu. Šumski vilenjaci, čija se zajednica nalazila tamo, su brzo postali svjesni njegove prisutnosti, ali nisu ga htjeli kontaktirati. Samo su ga promatrali. No kad su shvatili da nitko ne dolazi po njega niti da on ikoga očekuje, prišli su mu i iz sažaljenja prihvatili u svoje redove. Od tad Skip tamo izučuje druidski nauk.

Dugo vremena Skip provodi s vilenjacima, učeći njihov jezik i običaje. Također, brzo postaje sposoban druid, privlačeći divljenje i ljubomoru svojih starijih vilenjačkih kolega. Relativno je zadovoljan sa svojim životom, ali muči ga osjećaj srama zato što je napustio svoje prijatelje u Taurelu.

[Adrie Goldpetal|Mala Adrie] se rodi kada je on imao nekih trideset godina. Od malena je zanima Skip, jer je bio toliko drugačiji od drugih. Čudan mali živahan čovječuljak. Često bi dolazila igrati se s njime, i on bi je spremno prihvatio kad god bi za to imao vremena. Postala mu je kao mala sestra. No njezin nevini pogled na svijet i djetinjasta živahnost ga je podsjećala na njegove mlade dane, i on je sve više razmišljao o odlasku. Kada je dostigao 40 godina, odlučio se otputiti natrag u svoj rodni grad s ciljem da pomogne u njegovoj obnovi, ali Adrii obeća da će se vratiti svako nekoliko i pričati joj o svojim pustolovinama.

Kada se vrati u Taurel, vidi ga pod prividnom kontrolom vilenjaka i pravom kontrolom mafije na vrhu s njegovim nekadašnjim prijateljem. Osjećaji dužnosti i pravde prevladaju njegov osjećaj srama i krivnje, te on stane na kraj kriminalnoj organizaciji. Na putu stječe nekoliko prijatelja koji će mu kasnije postati vjerni suborci.

Kroz idućih nekoliko desetljeća Skip se bavi raznim poslovima, pomažući lokalnom stanovništvu da preživi posljedice ratovanja, a nikad se sam ne pridružujući nijednoj strani. On i njegova družina postaju slavni u južnoj pokrajini zahvaljujući brojnim herojskim djelima (većinom uklanjanje čudovišta i razbojničkih bandi).

Kada dinastija Bujold preuzme vlast, Skip sa zadovoljstvom vidi smanjenje unutarnjih nemira, i počnije se aktivnije baviti gušenjem pobuna onih koji su gladni moći, te iznošenje zahtjeva puka novim kraljevima. Tako postaje poznat na kraljevskom dvoru. Kad kralj Lionel I. dođe na vlast, on već tad drži Skip-a kao mudrom i dobrom osobom, te je uvijek otvoren za njegove savjete. Skip tada ima 80 godina.

Skip svoje devedesete godine provodi na bojištu, zajedno sa svojim suborcima, i kao pratnja u raznim diplomatskim misijama. Njegova herojska dijela šire glas o njemu i njegovim drugovima kroz cijelo kraljevstvo i šire, te postaje široko poznat. Skip-u je jedini cilj osigurati mir kraljevstva, i u tom je imao veliku ulogu. Kada dosegne svoju stotu godinu, kraljevstvo službeno prestaje sa svim ratovima, a Skip postane lvl 18.

Tih godina se također pojavljuju viteški redovi. Julius III., sin Lionela I., poziva Skip-a da prihvati viteški naslov. Julius je svjestan koliko je njegov otac poštivao Skip-a, i volio bi ga imati u svojim redovima. No Skip odbija, zaključivši da je doba njegovih junaštava završeno. Ne privlače ga ni plemićki status ni obveze koje viteški naslov nudi. Julius sa žaljenjem prihvaća njegovu odluku, i oni ostaju prijatelji. Skip se vraća u južnu provinciju, da ostatke svojih dana proživi mirno sa svojim prijateljima. Neki njegovi prijatelji, doduše, prihvate viteški naslov, i Skip provede neko vrijeme s njima.

Kada se konačno otputi natrag u svoj šumski dom, već su se počeli pojavljivati prvi avanturistički guildovi. Njihov cilj stjecanja vitevštva ga ne privlači, ali njihove metode ga podsjećaju na njegove mlade dane u Taurel-u, kada je sa svojim prijateljima rješavao svakakve tegobe svojeg grada. Ove misli ga prate i kad se opet vrati u svoju šumu. Tamo odlučuje da se po posljednji put vraća ovamo, te da će se oputiti u Taurel da osnuje novu vrstu guild-a i fokusirati na njegovo vodstvo. Osnuje guild Guild Alea, čija je svrha rješavati probleme Taurela u zajednici koja je poput obitelji.

S obzirom na slavne članove guilda Guild Alea (Skip i njegovi prijatelji), guild brzo postane popuralan i naraste na velik broj članova. Kada se osnuje Vijeće avanturističkih guildova, Skip mu pristupa kao meštar Alee. Tada mu je bilo 120 godina, a osjećao se kao da su mu 102.

Tijekom godina, većina njegovih prijatelja umire od starosti. Zahvaljujući svojim druidskim moćima, Skip preživljava, ali polako se počinje osjećati osamljen. No, trudi se zbližiti sa svim članovima svojeg guild-a, te im postaje kao stanoviti otac. Njegova obitelj ga drži da nastavi dalje unatoč gubitku mnogih starih poznanika.

Kada mu je bilo 140 godina, dočekalo ga je radosno iznenađenje. Mala Adrie je sada narasla u bistru djevojku. Nju je tijekom godina Skip toliko impresionirao svojim pričama da je otišla iz svoje šume čim je mogla, ponudivši svoju stručnost i znanje guild-u Guild Alea. Skip ju rado prihvaća, i ona mu postaje glavna pomoćnica u administracijskom dijelu upravljanja guild-a.


Halfling (Lightfoot) Druid (Circle of the Land - Forest) LVL 18
Chaotic Good

[rel://files/Skip.png]
Proficiencies:
Tool Proficiencies: Disguise kit, thieves’ tools
Armor: Light armor, medium armor, shields (druids will not wear armor or use shields made of metal)
Weapons: Clubs, daggers, darts, javelins, maces, quarterstaffs, scimitars, sickles, slings, spears
Tools: Herbalism kit

Feature: City Secrets
    You know the secret patterns and flow to cities and can find passages through the urban sprawl that others would miss. When you are not in combat, you (and companions you lead) can travel between any two locations in the city twice as fast as your speed would normally allow.
Lucky. When you roll a 1 on an attack roll, ability
check, or saving throw, you can reroll the die and must
use the new roll.

Brave: 
    You have advantage on saving throws against being frightened.
Halfling Nimbleness:
    You can move through the space of any creature that is of a size larger than yours.
Naturally Stealthy:
    You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.
Druidic:
    You know Druidic, the secret language of druids. You can speak the language and use it to leave hidden messages. You and others who know this language automatically spot such a message. Others spot the message’s presence with a successful DC 15 Wisdom (Perception) check but can’t decipher it without magic.
Land’s Stride
    Moving through nonmagical difficult terrain costs you no extra movement. You can also pass through nonmagical plants without being slowed by them and without taking damage from them if they have thorns, spines, or a similar hazard. In addition, you have advantage on saving throws against plants that are magically created or manipulated to impede movement, such those created by the entangle spell.
Nature’s Ward:
    You can’t be charmed or frightened by elementals or fey, and you are immune to poison and disease.
Nature’s Sanctuary:
    Creatures of the natural world sense your connection to nature and become hesitant to attack you. When a beast or plant creature attacks you, that creature must make a Wisdom saving throw against your druid spell save DC. On a failed save, the creature must choose a different target, or the attack automatically misses. On a successful save, the creature is immune to this effect for 24 hours. The creature is aware of this effect before it makes its attack against you.