﻿++ Session 1: Alein godišnji prijamni ispit
[:toc:]
*Datum održavanja: 22.01.2017., 29.01.2017.*

*Sudionici:* [Likovi|svi]

+++ Setup
*Datum: Godina 965 po Lumos-u, 3'rd of Ches (The Claw of the Sunsets)* - rani Ožujak

Na početku, likovi jedan po jedan imaju individualni intervju s [Adrie Goldpetal|Adrie], jednom od zaposlenica u guild-u. Ona ih ispituje za ime, razlog pristupanja guild-u, njihovu specijalnost, i da kažu nešto o sebi. Ako ne spomenu, pita ih što misle da je njihova mana. Kada ima intervju sa zgodnim barbarima, počinje se meškoljiti.

Svi plate ulaznu cijenu od *1g* za organizaciju ispita.

Nakon intervju-a, svi pristupnici (maksimalno njih 30-ak) se sastaju zajedno i upoznaju meštra [Skip|Skipa] ispred ulaza u guild. Nalazi se tamo zajedno s Adrie i nekoliko drugih članova guild-a. On im objasni da će se prvi dio ispita održavati na daljoj lokaciji, i scena se mijenja.
[:rel:top]
++++ Šuma kušnji
*Scena:* dan (oko podne), normalan teren s mjestimičnim polu-zaklonima (grmlje, težak teren) i punim zaklonima (drveće).

*Opis:* Dan je. Nakon kratke šetnje van grada, nalazite se u dolini omeđenoj šumom. _(Pastiri vole ovaj kraj, stoga je raslinje nisko izuzev šume koja kao da je naglo prekinuta.)_ Pokoje drvo stoji na osami u dolini niske trave i sitnog cvijeća, dok malo dalje dolinu omeđuje red grmlja i duboka šuma. Potok protiče kroz šumu i izviruje u dolinu gdje pastiri napajaju svoje životinje. U daljini se naziru nekakve kamenite ruševine.

Skupina pristupnika stiže u ovu dolinu skupa sa Skip-om, Adriom i još nekoliko članova guilda (healeri).

*Radnja:* Ovo je mjesto gdje će pristupnici pokazati svoje sposobnosti u tipičnom zadatku eliminacije opasnih zvijeri. U šumi se nalaze nekoliko vrsta divljih zvijeri koje će na znak iskočiti na vidjelo i napasti koga god ugledaju. U stvarnosti, to nisu prave zvijeri nego "fey spirits", i prilikom smrti samo nestanu. Pristupnici mogu raditi što god hoće, a suci će ih ocjenjivati s obzirom na njihov učinak. _(u stvarnosti, nema ocjena niti bodova, nego pristupnike promatraju Spike, Adrie i još nekoliko viših članova guilda u svrhu podjele pristupnika u grupe)_

*Neprijatelji:* Boar (50XP), Flying Snake (25XP), Panther(50XP), Poisonous Snake (25XP), Wolf(50XP)

*Borba:* Boar, Wolf i Poisonous Snake prvi iskoče. Kada prvi monster umre, zamijeni ga Flying Snake koja izleti iz šume. Kada ostane samo jedan monster živ, Pantera iskoči iz grmlja.

*Rezolucija:* Kada borba završi, vrate se Skip i ostali, čestitaju na dobro obavljenom poslu i daju svima da se odmore. Healeri pohitaju ozlijeđenima, prvo se baveći težim ozljedama. Iznesu sendviče i piće. Ovdje najave da će idući test zahtijevati rad u grupama, i objave tko je u kojoj grupi. Savjetuju da se ljudi grupiraju odmah i malo bolje upoznaju tijekom odmora. Oni koji nisu ni u jednoj grupi, otpadaju u ovom stadiju. _Pola ljudi se pokupi i otiđe, umorno i razočarano. Skip i ostali se pridruže sendvičima, te razgovaraju slobodno s pristupnicima ako im ovi priđu._

Nakon odmora, svi skupa odšetaju do ruševina koje se nalaze malo dalje na brdu.

*Potencijalne nagrade:* 200 XP-a (40 XP-a svakome).
[:rel:top]
++++ Labirint tajni

*Scena:* isprva vanjština ruševina, a zatim unutrašnjost - potpuni mrak; kameni podovi, zidovi i stropovi u relativno dobrom stanju.

*Opis:* Na vrhu brda nalazi se ruševina od kamenja. Megalitna arhitektura. Veliki, sivi kameni blokovi razasuti po popločenom kamenom podu, neravan i oštećen od vremena. Na nekoliko mjesta se naziru prolazi u dubinu.

Kada uđu kroz ulaz, dočeka ih mrak podzemlja. Pod svjetlom se vide samo zavojiti hodnici od velikih kamenih blokova. Mjestimično se mogu naći mala oštećenja ili izvor podzemnih voda, ali većinom je cijeli kompleks u začuđujuće dobrom stanju, kamen ulašten do sjaja.

*Radnja:* Skip ovdje zaustavi skupinu, i objasni idući test. Pod njim se nalazi veliki kompleks labirinata, čija je svrha dan danas nepoznata. No svaki kutak tog podzemnog prostora je već istražen, stoga mjesto nije jako popularno. Skip je odlučio iskoristiti potencijal ovog prostora tako da ovdje održi dio ispita. Labirint je podijeljen na sektore. Svaka grupa će dobiti jedan sektor da istraži. Cilj grupe je doći do kraja labirinta u što kraćem roku. Na kraju labirinta nalaze se velika kamenita vrata iza kojih čeka vodič, čija je uloga da mjeri njihovo prolazno vrijeme i da izvede grupu natrag van ruševina. Labirint sadrži razne dodatne izazove, ali svaki od njih nagrađuje one koji ih uspješno prođu, na neki način im pomažući da brže dođu do cilja. Na grupi je da odluči isplati li se riskirati dodatno trošenje vremena za te potencijalne nagrade. _(u stvarnosti, njihovo prolazno vrijeme nije bitno, nego Spike-a i ostale zanima kako će se pristupnici ponašati u situaciji sa ograničenim vremenom)_ Svi imaju 5 minuta da se dogovore kako će se organizirati u labirintu, i onda kreću.
[:rel:top]
+++++Setup
Drugi Encounter uključuje podzemni labirint sa zamkama i tajnama. Cilj igrača je proći labirint što brže. 

U labirintu su 5 odredišta: 3 duha, trap room i puzzle room. Puzzle room se mora proći da bi se izašlo. Svi roll-aju d20 i rezultat se zbraja. Zbrojeni rezultat određuje na koje odredište su naišli (po rasponima koji predstavljaju jednoliku vjerojatnost). Odredišta su složena po međusobnim udaljenostima:
<<|
 | *Trap* | *Emo* | *Dwarf* | *Elf* | *Puzzle*
*Ulaz* | 1 | 1 | 2 | 2 | 4
*Trap* | x | 1 | 2 | 1 | 3
*Emo* | 1 | x | 1 | 2 | 3
*Dwarf* | 2 | 1 | x | 1 | 2
*Elf* | 1 | 2 | 1 | x | 2
*Puzzle* | 3 | 3 | 2 | 2 | x
>>
[https://jsfiddle.net/Fluid/bd1xrsg2/embedded/|Probability distribution calculator]

Udaljenosti u tablici odgovaraju vremenu koje igrači potroše tražeći pravi put, crtajući mapu, radeći oznake i vrteći se krivim putevima. Za proći jednu jedinicu udaljenosti treba otprilike 10 minuta.
Doći do mjesta gdje su prije bili je trivijalno, ako su opisali da rade mapu ili ostavljaju oznake. Vrijeme potrebno da dođu tamo je tada dvostruko manje (jer se ne petljaju po krivim putevima). Ako to ne rade, moraju rollati Intelligence (zapamtili su niz skretanja) ili Wisdom (imaju dobru orijentaciju), DC 17. Na fail, šansa za doći do mjesta gdje su prije bili je jednaka kao da tamo nisu ni bili.

Šanse za doći negdje gdje nisu bili su obrnuto proporcionalne udaljenosti odredišta.
[:rel:top]
+++++Klopka
Na kraju hodnika nalazi se ulaz od teških kamenih vrata. Na samim vratima uklesan je simbol koji predstavlja uzorak za rješenje [Klopka sa strelicama|klopku] koja ih čeka.

Ako grupa uspješno prođe klopku, na kraju nalazi na jednu malu sobu s kamenim postoljem visokim do struka. Na njemu se nalazi papir sa idućim natpisom:
Žao nam je, ali ovo nije kraj labirinta. No evo vam utješna nagrada za uspješno prevaljeni izazov - savjeti za ostatak labirinta:
1.) Duhoviti ljudi odgovaraju sa "Da, i...", a ne sa "Ne, ali...".
2.) Važno je imati oko za detalje. Ali imajte na umu da nijedno oko ne vidi svugdje.
[:rel:top]
+++++Duhovi

Duhovi koji se nalaze unutra mogu igračima pomoći ili odmoći, ovisi svidi li im se grupa ili ne. Duhovi su stereotipni, i na igračima je da prepoznaju stereotip i iskoriste ga da se duhu približe. Iterakcija s duhovima igračima troši vrijeme, između 5 i 10 minuta.

Prvi duh je Emo. Nosi crnu odjeću, neprirodno bijelo lice sa crnilom oko očiju, piercing na nosu, dugu zalizanu crnu kosu. Umišljena, nikad se ne smiješi. Stalno govori o boli (potajno je mazohist). Drži crnu knjižicu poezije (stila Nevermore) u koju samo gleda (više joj služi kao rekvizit). Ako joj se igrači pridruže u emovanju, ona "podijeli bol s njima". Likovi osjete oštru bol u glavi na par sekundi, i onda im se mapa mjesta pojavi u glavi, zajedno s lokacijom izlaza i svih duhova. Ali Emo ne zna gdje su zamke, jer su one friško postavljene. Zna samo da uključuju padove, oštre šiljke, kiselinu, vatru... ili barem tako kaže (i priželjkuje, napaljuje ju). Ako ju igrači naljute ili pokušaju razveseliti, ona proglasi "vi me ne razumijete" i cast-a darkness spell. Ako se igrači ne znaju riješiti spell-a, izgubit će nešto vremena dok svi ne izađu. Koliko vremena izgube ovisi o tome koliko su organizirani (između 5 i 10 minuta). 

Drugi duh je aristokrat. Umišlja si da je vrlo inteligentan (zapravo nije) i ima vrlo profinjene ukuse. Privlači ga urednost, pristojnost i uglađen govor. Odbija ga nečistoća, divljaštvo i priprostost. Obučen je bogato, s puno detalja (valovi na ovratniku, broševi, biserni gumbi, napudrana perika, lažni madež ispod oka, teška šminka). Koristi pretjerano kićeni govor i velike riječi kojima ne zna pravo značenje. Ukoliko mu se igrači ulizuju i postave kao on, kaže im pravi smjer za izlaz i poželi "sreću" - svi dobe advantage na svoj prvi idući d20 roll. Ako ga uvrijede ili razotkriju, proglasi ih prljavima, zalije vodom i uputi ih dwarfu, da se druže sa "svojima".

anchor: dwarf_ghost
Treći duh je lagano neuredan, malo prljav, znojan, ali veseo dwarf. On sam izgleda kao komprimirana masa mišića, pokrivena njegovom napetom tamnoputom kožom, omotana u nekad bijelu konobarsku pregaču i prljave kratke hlačice od smeđe brušene kože. Proćelav je, ali ima veličanstvenu smeđu bradu. Tame crne oči su širom otvore i gledaju s veseljem. Od nekuda može magično izvući neograničenu količinu krigli piva. Voli otvorenost, snagu i veselje. Ne voli introverte i sumnjičave ljude. Nudi ljudima piće i hoće se napiti s njima. Ako netko snažan prihvati, ponudi mu obaranje ruku. Ako taj i pobijedi, uputi ih na izlaz, pozove da dođu opet, i svakom da čvrsti stisak ruke, koji osobi da 5 temporary HP-a. Ako se netko napije, ima disadvantage na sve, i cijela grupa je izgubila 10 minuta vremena. Ako se grupa ne svidi dwarfu, izgleda razočarano i uputi ih aristokratu, jer možda će mu njegovo društvo više pasati.
[:rel:top]
+++++Puzzle soba
Ulaz u sobu su drvena vrata. Iznad vrata nalazi se uklesano veliko oko, sa ljudskom lubanjom umjesto zjenice. Kada likovi uđu u sobu, mogu odmah vidjeti stup ispred sebe, nakupinu razrušenog kamenja, zidove na desnoj strani i još jedno oko na lijevoj strani, u ravnini stupa. Znakovi oka u ovoj sobi su magični i rade 1d6 psychic damage-a u 6 sekundi osobi čija je glava u području direktno ispred oka. Štit može smanjiti ovaj damage na 1d4, ali samo ako je pozicioniran točno između glave i oka. Veliki kameni blokovi u potpunosti blokiraju ovu magiju. Ukoliko nešto neživo uđe u područje magije, oko ne reagira. Čim nešto živo uđe u područje, oko se očinje sjajiti ljubičastim sjajem, a to biće koje je aktiviralo magiju osjeti nelagodu. Oko je to sjajnije što je glava bliža području oštečenja.
Na podu ispred oka nalazi se mrtvi štakor. DC 15 Perception check ga otkriva, a DC 10 Investigation otkriva da mu je glava eksplodirala.
Na stražnjoj strani sobe nalazi se cijeli niz znakova na zidu, kao i veći broj mrtvih štakora na rubu jedne i druge strane zone smrti. Svima je glava eksplodirala. Ovaj dio prostora se ne vidi s ulaza, treba se proći s jedne strane da bi se prizor vidio. Na desnoj strani nalazi se veliki kameni blok u obliku trokutne prizme, sa držačima na plohi hipotenuze. Guranje kamenog bloka zahtjeva DC20 Strength (Athletics) check, jedna dodatna osoba koja pomaže daje advantage na roll, a tri i više osoba je automatski uspjeh. Kameni blok je dovoljno debeo da blokira većinu magije, ostavljajući samo blagu glavobolju.
Na desnom čošku sobe nalaze se velika kamena vrata sa rezbarijama ptica u letu, naznačujući kraj labirinta.
[Puzzle soba|Slika]

*Rezolucija:* Likovi dođu do kraja labirinta. Sa sobom nose bilo kakve buff-ove koje su stekli na putu, ili pate od ozljeda nakon upadanja u zamke. Kada otvaraju vrata koja su na kraju, dočeka ih scena ovisno o tome koliko im je vremena trebalo da dođu.

*Potencijalne nagrade:* 50 XP-a (10 XP-a svakome) za svakog uspješno pridobljenog duha.
100 XP-a za brzi prolaz labirinta (do 45 min), 50 XP-a za srednje brzo (do 65 minuta).
[:rel:top]
++++ Neočekivani neprijatelj

*Scena:* dugačak hodnik sa stupovima, mračan izuzev torch-a kojeg vodič ima (je imao) sa sobom.

*Opis:* Ista kamena arhitektura iz labirinta se dalje nastavlja dugačkim hodnikom. Hodnik je zapravo dosta širok, ali stupovi koji stoje sa svake strane hodnika smanjuju vidljivost.

*Radnja:* Ovisno o rezultatu prošlog dijela, likovi naiđu na tri moguće situacije:

    1. *Ako su likovi potrošili do 45 minuta:* [John Farcrest|Vodič] stoji ispred vrata, prtljajući po nekoj napravi u svojoj ruci. U drugoj ruci nosi karton na koji nešto zabilježi, te zatim podigne pogled i osmjehne se grupi. _(naprava je kronometar, a zapisivao je vrijeme koje je trebalo grupi da prođe labirint.)_ Njegova baklja nalazi se zataknuta na stupu kraj njega. Iza njega, bez njegovog znanja, [Vampire Thirster|blijeda spodoba] mu se prišuljava ogoljenih očnjaka. On je smrznut na mjestu, a njegove krvave oči su široko otvorene i uperene u nenadane goste. No, sekundu poslije zgrabi vodiča i položi svoje šiljaste nokte na njegov vrat, uzimajući ga za taoca. Slijedi razgovor sa grupom, tokom kojeg se pokušava što više udaljiti od grupe. Kada dođe 30ft-a daleko, podigne svoj kaput i pusti šišmiše. Tu počinje borba, i u svojoj rundi vampir pokuša pobjeći iza što daljeg stupa i sakriti se. Nakon toga se penje po stupu i sakriva u tami između stropa i zida. Pokušava se približiti grupi i zaskočiti slabije članove iz mraka dok su preokupirani šišmišima. Vodič je na podu u nesvijesti od šoka.
    2. *Ako su likovi potrošili do 65 minuta:* Ispred vrata na podu leži vodič, mrtav. Vampir je nad njim i siše mu krv. Karton i kronometar se nalaze na podu, a baklja još uvijek zataknuta za stup.. Vampir pogleda grupu iznenađenim pogledom, glupavo se osmjehne, i onda ispusti šišmiše. Dok šišmiši lete na prvog najbližeg, vampir napada one koje izgledaju slabije.
    3. *Ako su likovi potrošili više od 65 minuta:* Vodiču koji ih je trebao dočekati nema ni traga. U zraku se osjeti blagi metalni miris. Pasivni Perception 10 otkriva mrlje na podu, DC 15 Investigation otkriva da je to krv, a malo istraživanja će otkriti truplo vodiča sakriveno u mračnom čošku iza prvog stupa, zajedno sa svojim stvarima i ugašenom bakljom. Ukoliko grupa otkrije truplo vodiča, ili se netko izdvoji više od 30 ft-a od grupe, jato šišmiša dođe niotkud i napada grupu. Kada su svi preokupirani šišmišima, vampir napada najizdvojeniju osobu iz mraka, zaskočivši ga sa stropa. Uspješan Perception check protiv vampirovog stealth check-a bi mogao to spriječiti. Ako to eksplicitno ne kažu, prije borbe sa šišmišima igrači koriste Passive Perception, a u borbi neka rollaju.
    
*Neprijatelji:* Swarm of Bats (50XP), Vampire Thirster (200XP)

*Rezolucija:* Kada borba završi, šišmiši su mrtvi, a vampir mrtav ili uhvaćen. Ispitivanje može dovesti do aktiviranja kristala i smrti vampira. 

*Potencijalne nagrade:* 250 XP-a (50 XP-a svakome)
Ako vampir ostane živ i biva zatočen od strane guild-a: 200 XP-a (40 XP-a svakome).
Ako vodič ostane živ: 50 XP-a (10 XP-a svakome) i po jedan potion svakome po želji (vidi Rasplet).
Na vampiru (ako ga likovi ubiju/uhvate i pretraže): zlatni lančić (vidi [Vampire Thirster]) , 15sp-a, Potion of Invisibility, prazna bočica (vidi [Vampire Thirster])
5 Potion of Healing (nagrada za prolazak ispita).
[:rel:top]
+++ Rasplet

Ako likovi jave prvoj osobi koju nađu što se dogodilo, ispit se prekida. Likovi budu uvedeni u sobu za promatranje. To je po izgledu najbolje očuvana prostorija u ovim ruševinama, te je natrpana stolovima s raznim magičnim uređajima koji su donešeni izvana. Na stražnjem zidu veći broj čarobnjaka sjedi na stolcima koncentrirajući se na čarolije (Clairvoyance). Ostali ljudi su uznemireni, jurcajuči okolo po naredbama staložene Adrie. Ona smjesti likove na klupice i kaže im da pričekaju. Malo kasnije, dođe zajedno sa Skipom do njih.

Ako je vodič mrtav, Skip je ozbiljan, a inače je samo ljubazan. Ispričava se zbog sigurnosnog propusta (ovo je bio prvi put da je netko napao regrute). Ako je vodič spašen, zahvali im se od srca, i kaže da za nagradu si odaberu po jedan potion iz njihove lokalne zalihe (common i uncommon potion-i). Pita ih znaju li zašto ih je vampir napao, i obeća da će osobno istražiti slučaj. Ako likovi nemaju korisnih informacija, natukne da je možda u pitanju rast tenzija među guild-ovima. Guildovi si uvijek međusobno konkuriraju, što nekad zna prerasti i u sukobe.

Na svjetliju notu, Skip spomene da je zadnji dio ispita trebao biti grupni sparring protiv jednog od jačih članova guild-a, ali s obzirom na protekli događaj Skip smatra da su likovi prošli ispit. Jedina druga grupa koja je prošla labirint nije prošla taj zadnji dio, tako da su oni jedini novi članovi guilda. Čestita im, i želi dobrodošlicu u guild. Svakom udijeli nagradni Potion of Healing (mimo već obećanih potion-a) i poželi sreću u daljnjim avanturama.

Ako je vodič živ, i on svrati na kraju i zahvali se junacima što su ga spasili. Poziva ih sve na večeru i cugu na njegov trošak.
[:rel:top]
+++ Događaji
Vorko je uspio zavesti Adrie. Mišičavi barbariani je i dalje fizički privlače, ali njihova osobnost ju zabrinjava (pogotovo human). Prema njima će se odnositi normalno dokle god ne ističu svoje fizičke atribute. Prema Vorku će biti malo prisnija.
*Šuma kušnji:* 
Riješena (200XP-a); So-ke prijeteći ispitivao Gom'bar-a o orcima koji napadaju sela. Ovaj mu nije znao reći ništa više osim da su i njegovo selo napali. So-ke je čuo za Gom'bar-ov klan, njihovi klanovi su znali trgovati međusobno. U dobrim su odnosima bili, pa je i So-ke malo smirio strasti.
*Labirint tajni:*
Prvo su naletjeli na Emo duha. Izgledalo je kao da će ih otkantati, ali igrači su uspjeli dokučiti taktiku. So-ke je krenuo pričati o svojoj bolnoj prošlosti, i onda je potpuno kolabirao, plačući u fetalnom položaju na podu. To je bilo dovoljno za uspjeh.
Znajući sad sve lokacije, odlučili su riskirati i otići do idućeg najbližeg duha. To je bio dwarf. Isprva su bili malo sumnjičavi, ali zaključili su da je dwarf iskren te spremno prihvatili njegove ponude piva i igri. Gom'bar je pobijedio u obaranju ruku, i dobili su +5 temp. HP-a.
Zatim su otišli do izlaza. Dobro su riješili zagonetku, makar su unutra potrošili neko vrijeme. Također, [Elli|Vorkova mačka] je nehotice ušla u prostor oka (privučena mrtvim štakorom) i zadobila tešku ozlijedu. Neće se vratiti još neko vrijeme.
Vrijeme prolaska kroz labirint: 40 min.
*Neočekivani neprijatelj:*
Scenarij 1 je aktivan, uhvatili su vampira prije napada. Vampir je uzeo vodiča za taocca i uspješno se odmaknuo. Zatim se sakrio iza stupa i pustio swarm of bats.
*PAUZA:*
Red inicijative: So-ke(21), Swarm of Bats(17), Kaliki(16), Fangora(15), Vampire Thirster(9), Vorko(8), Gom'Bar(4)
Potezi:
    0. Vampir se sakrio iza stupa. STEALTH 17. Planira se popeti uza stup na svoj idući red.
    1. So-ke je došao do vodiča i tu stao.
    2. Swarm of Bats je izletio do So-ke-a, ozlijedio ga je za 7 piercing damage.
*NAKON PAUZE*
    * healali su vodiča i rekli mu da bježi
        * on je pobjegao van da dovede pomoć
    * vampir je napao Vorka (uz povik "Umri, Bujold!"), popio mu malo krvi i probo kanđama
        * Vorko pada onesviješten
    * So-ke ubije šišmiše, Gom'bar ubije vampira
    * Pri smrti vampira, ogrlica se aktivira i zaslijepi ih sve, a vampira pretvara u kamen i prah

Nakon bitke, dolazi vodič s Adrie i još nekoliko starijih članova guild-a. Povedu ih do nadzorne sobe, healaju sve (i Vorko se osvijesti), te im kažu ta pričekaju. Onda dođe skip i priča s likovima kao planirano. Pritom mu oni sve kažu, i još k tome Vorko prizna da je Bujold. Ne otkriva puno više o tome (Adria će s njime privatno korespondirati kasnije, da ga pita treba li obavijestiti kraljevsku obitelj). Skip im obeća da će istražiti što može (a ne može puno jer je vampir mrtav i likovi mu nisu rekli za stvari koje je nosio).
[:rel:top]
+++ Nagrade

*Šuma kušnji:* 
200 XP-a
*Labirint tajni:*
200 XP-a, 5 temp. HP
*Neočekivani neprijatelj:*
300 XP-a, 8 Potion of Healing, Potion of Climbing, Potion of Gaseous Form, Potion of Invisibility, Zlatni lančić, prazna potion bočica, 15sp
[:rel:top]