﻿++ Opus Magni

Jedan od največih guild-ova u [Lumecia|Lumecii], orijentira se primarno na spellcast-ere. Veći dio članova su Wizard-i i Sorcerer-i, sa manjim brojem drugih klasa koje imaju magično-orijentirane specijalizacije. Clerici, Paladini i Druidi su uključeni u manjem broju jer zbog svojih snažnih uvjerenja pristupaju magiji na drugi način, te im ne odgovara istraživački i eksperimentalni duh guild-a. Warloci su zabranjeni, kao i bilo koji oblik nekromanskih i demoničkih čarolija.

Guild je osnovao Wizard po imenu Astronimus, koji je odrastao u jednog od največih čarobnjaka svojega vremena. Zbog njegove reputacije, njegov guild je večinom privlačio poslove koji zahtijevaju bar nekakvu magičnu intervenciju. Zbog toga su u guild olazili primarno spellcaster-i, i tako je tradicija nastala. No, za razliku od većine magičnih institucija, pripadnici guilda su bili skromni ljudi koji se nisu shvaćali preozbiljno. Određena razina odgovornosti je nužna kod ljudi koji upravljaju magičnim močima, ali Astronimus nije htio da njegovi kolege u guild-u izrastu u stroge, uštogljene ljude koji ne vide dalje od svojih knjiga.

Guild-u je ime nadjenuo sam Astronimus. Kada ga se pita zašto je odabrao to ime, njegov odgovor je uvijek "zvuči kul".

Trenutni meštar, [Archmage Marie], trudi se nastaviti tradiciju guild-a koji je preuzela, te je mnogi članovi guild-a gledaju poput majke.