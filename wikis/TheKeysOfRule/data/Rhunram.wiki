﻿++ Rhunram
[alias:Narbmaurr; Dawn Peaks]

Rhunram je naziv za planisnki lanac koji se proteže od sjevera kraljevstva do njegove sredine, a dijeli istočnu (vilenjačku) i zapadnu (ljudsku) provinciju.

Samo ime Rhunram dolazi iz vilenjačkog jezika, a značenje mu je "Istočni Zid". Planina ima i druga imena: Narbmaurr (Scar mountains) među patuljcima, Dawn Peaks među ljudima istoka.
Rhunram je dom [Patuljci planine Rhunram|patuljaka], te im služi kao izvor drva i lovine u nižim predjelima, te rude u svojoj unutrašnjosti. Također, kao prirodna prepreka između zapadne i istočne provincije, planina pruža dodatan prihod patuljcima kroz taksu za korištenje njihovim tunelima i planinskim prolazima koje održavaju.

Patuljci u Rhunram-u su vrsni artisani. Njihovo umjeće gradnje pomalo je umanjeno njihovom jednostavnom estetikom - veliki kameni blokovi i prave linije. Ali velebni podzemni gradovi patuljaka oduzimaju dah svojom nekarakterističnom veličinom. Gradovi su im narasli do te razine da geološki događaji unutar planine imaju utjecaj na stabilnost građevina, s toga je velika pozornost posvećena provjeravanju i održavanju glavnih potpornja unutarnjih struktura. Svaki potporanj ima redundantni backup, tako da bi samo istovremeno uništenje većeg broja potpornja moglo uzrokovati bilo kakvu veću štetu.

anchor: dwarf_arts
Uz građevinsko umjeće, patuljci su i vrsni kovači. Imaju poseban talent za izradom magičnih oružja, oklopa i drugih metalnih predmeta poput prstenja i remena. Svake godine održavaju aukciju za prodaju svojih najvrijednijih tvorevina. Ta aukcija je jedino doba u godini kada se unutar planine nalazi veći broj stranaca. Aukcija često privlači i pljačkaše, ali kontrole su stroge na ulazima u planinu i samo najvrsniji pljačkaši se mogu provući unutra neopaženo.