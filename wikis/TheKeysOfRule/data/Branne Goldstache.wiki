﻿++ Branne Goldstache

Branne Goldstache vodi istraživačku organizaciju [Brando Expeditions]. Ona zna lokaciju mnogih ruševina (sve istražene, osim ove) i ima brojne izvore koje koristi da se prva domogne novih čim budu otkrivene. Također zna sve što i drugi patuljci znaju, uključujući [Patuljci planine Rhunram#dvarf_vampire_knowledge|priču o vampirima]. Ima veliko znanje o povijesti zemlje, uključujući sve važne informacije s [Lumecia|vremenske crte] koje su ikad bile javno dostupne.


Branne zna malo magije, dovoljno za Detect Magic i Comprehend Languages. Ali ona nije borac, kao ni nitko od njenih sudruga.