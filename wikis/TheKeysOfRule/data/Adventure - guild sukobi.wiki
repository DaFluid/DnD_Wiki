﻿++ Adventure - guild sukobi

//TODO pomakni datum
*Datum: Godina 965 po Lumos-u, 14'th of Ches (The Claw of the Sunsets)*
    
    * Quest: Lokalni feudalni gospodar [Lord Millard Falconcrest] izdaje otvoreni poziv svim lokalnim guildovima i nezavisnim avanturistima da ulove i ubiju čopor Allosaurusa koji je napao životinje i seljane na njegovim zemljama. 500g nagrada


    * session ideja: neki hunting quest. po završetku ih zahvati opsada - suparnički guild ([Guild Feniks|Feniksi], iz grada Kilmir, dva dana hoda prema Dawncross-u) koji je isto ciljao na taj quest ih odluči ukloniti, okriviti lovinu za njihovu smrt i preuzeti nagradu za sebe. Ako ih party primijeti prijevremeno, jedan pričljivi među njima ih pokuša uvjeriti da su ih samo promatrali da vide da nisu neki kriminalci ili lovokradice. Ako su uhvaćeni u laži, uslijedi bitka. Inače, "nude" da riješe quest zajednički i podijele nagradu (a zapravo gledaju priliku da im zabiju nož u leđa). Ukoliko igrači odbiju, uslijedi bitka. Ovisno o lokaciji bitke, glasni zvuci mogu privući zvijeri i pretvoriti bitku u trostrani okršaj (gdje će Feniksi pokušavati staviti igrače između sebe i zvijeri, a zvijeri napadati one koji se izdvajaju od grupe, te one najbliže ako takvih nema).
        * brbljavi (Spy) priča sve što mu taj tren odgovara, a ostali su ili nasilne, pohlepni i/ili vlastohlepni. Scout-ica je iznimka - nije samaritanac, ali nije ni okrutna kao ostatak njezine skupine.
        * Spy - Fili, halfling; Scout - Serena, elf; Acolyte - Adronus, human; Thug - Bo, human; Drow - Hashilli, drow 
        
        
Dobar skup neprijatelja: Acolyte (50XP, MM342), Drow (50XP, MM342), Scout (100XP, MM349), Spy (200XP, MM349), Thug (100XP, MM350) - od 500XP-a do 1000XP-a, duplo za svakog zarobljenog

U borbi - Drow casta Faerie Fire odmah, i puca na one koji su failali roll da poništi svoj disadvantage. Ako nekog uspava, promijeni metu. Ako je u gabuli, casta darkness i pokuša pobjeći.
Scout gađa ili zvijeri ili primarno Gom'Bar-a i So-ke-ja (prvo se praveći da joj je strijela ispala), a bježi ako ostane sama ili je na niskim health-ima. Ako je uhvate, predaje se.
Thug i Spy su nemilosrdni i uživaju u tuđoj boli.
Acolyte je sadističan izdaleka, ali uspaničen kad mu se približe. Uvijek čuva 1 heal za sebe i bježanje.

Lovina: 2 [Young Allosaurus] (50XP) i 1 Allosaurus (450XP)

U borbi - Allosaurus juriša prvi, a mali ga slijede. Mali napadaju prvog neprijatelja koji je najbliži lovini starijeg. Ako netko ubije jednog od mlađih, stariji popizdi i fokusira se na ubojicu.



Priča: Dinosauri izvorno potječu s otoka Devil's Hoof. Na kontinent su ih dovezli gusari, a kupovali su ih ljudi kao egzotične ljubimce i borbene životinje. No, Dinosaure je teško trenirati, i bilo je samo pitanje vremena kada će koji pobjeći. Bez značajnih predatora, dinosauri se brzo množe i dominiraju staništem, radi čega je lov na njih uvijek legalan. Ponekad napadnu i naselja, gdje su u kratkom roku sposobni proždrati ogromnu količinu stoke.


