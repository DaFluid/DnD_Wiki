﻿++ Session 3
[:toc:]
*Datum održavanja: 12.03.2017.*

*Sudionici:* [Gombar|Gom'Bar], [Soke], [Kaliki], [Fangora]
+++ Prolog
*Datum: Godina 965 po Lumos-u, 6'th of Ches (The Claw of the Sunsets)*
Kako likovi prolaze kroz guild, ljudi ih veselo pozdravljaju. Pokoji kaže nešto poput "dobar posao s onim banditima". Vijesti se šire brzo u guild-u.

Adrie potiho pozove Vorka, i ispriča se što je nagrada u prošlom quest-u bila mala s obzirom na rizik. Da im se oduži na neki način, sačuvala im je quest s pristojnom nagradom. 

_Arheološka zajednica [Brando Expeditions] zove avanturiste da budu pratioci arheološkog tima u istraživanju nedavno otkrivene ruševine sjeverno od Taurel-a. Prva ekspedicija nije došla daleko zbog undead-ova u prostorima ruševine. Zadatak: ukloniti sve prijetnje u ruševinama. Uvjet: sve nađene umjetnine prelaze u vlasništvo Brando Expeditions-a. Nagrada je 1000gp, ili 5000gp ako uspješno nađu i dostave barem jednu vrijednu umjetninu._

Njihova predstavnica, [Branne Goldstache], trenutno odsjeda u Taurel-u u inn-u Propeti Medvjed, čekajući na odaziv guild-a.
[:rel:top]
+++ Upoznavanje s Branne
Propeti Medvjed je staromodan inn sa jednako staromodnim innkeeper-om. Igrači se u inn-u mogu raspitati za Branne, a stari innkeeper ih drhtavom smežuranom rukom, bez riječi, uputi u smjeru stola gdje Branne nešto piskara, kraj nje tri prazne krigle. Branne je snažno građena patuljkinja, pješćano plave kose zapletene u dvije pletenice i blijede kože prošarane pjegicama. Kada joj se igrači približe, pogleda ih s isčekivanjem i pita tko su. Kad sazna da su od Alee, predstavi se službeno i ponudi im piće, te im objasni situaciju.

Objašnjava da je [Brando Expeditions] arheološka organizacija koja ima svoj začetak još u doba kralja Lumosa, a danas se bavi otkrivanjem i prodajom vrijenih antikviteta. (vidi [Brando Expeditions|stranicu] za detaljnije odgovore ako igrači pitaju) Branne vodi organizaciju, i san joj je otkriti više o povijesti ovih prostora, poglavito o životu dwarf-ova. Kroz svoj rad dobila je određene indikacije da da su drevni patuljci bili rašireni na puno većem području nego danas. Ona želi naći više dokaza za tu tvrdnju, i neki trag o tome zašto su se nenadano povukli i izgradili tvrđave u dubini planine Rhunram. (Odgovor: vampiri)

Ostatak ekipe kampira nedaleko od ruševina. Trebat će im par dana da došeću do tamo - ruševine su jugoistočno od Taurela, u smjeru pustinje [Had].
Kad dođu do kampa, upoznaju ostatak ekipe: mladi halfling i halflingica (Marfire i Frona) koji su uvijek skupa, jedan dragonborn s naočalama (Alihadur) i jedna tiefling-ica s kožnim bičem i šeširom ([Indie Anne Jax]).
[:rel:top]
+++ Ulaz u ruševinu
Od drevne zgrade je tek malo što ostao. Ruševine se prostiru u području od oko 150m^2, i većina ih je sravnana s tlom. Pokoji kameni stup još uvijek se odupire gravitaciji, bacajući sjenu ranojutarnjeg sunca na mahovinom prekrivene kamene ploče. Jedino se središnji dio još nekako drži, a i taj izgleda kao da bi se svaki čas mogao raspasti. Na stražnjoj strani zgrade vidi se jedan veliki urušeni dio, api prednje lice zgrade, skupa s ulazom, još uvijek stoje. Ispred ulaza je naslagano nešto kamenja. To su naslagali Brandovci nakon prve ekspedicije, ne želeći dopustiti stvorovima iz dubina da ih slijede.
[:rel:top]
+++ Čišćenje unutrašnjosti
*Radnja:*
Unutrašnjost zgrade je zapravo dosta mračna, izuzev nekoliko mjesta gdje prodire sunčeva svjetlost. Veći dio prostora je vidljiv, ali tamni kamen ne odbija puno svjetlosti, te iza stupova i u mračnim kutevima vrebaju sjene.

No prvi neprijatelji koji se mogu ugledati su četiri zombija koji besciljno lutaju prostorom, i dvije živuče šake koje puze po podu.
*Neprijatelji:* 3 Crawling Claw (10XP), 1 Shadow (100XP) i 3 Zombie (50XP)
*Borba:*
Zombiji ugledaju likove čim otvore vrata i napadaju uvijek sebi najbližu osobu. U slučaju jednako bliskih meta, napadaju glasnije.
Sjene vrebaju u mraku i čekaju da im se netko približi, i onda napadaju. Klone se svjetla, te će ući u svjetlo napadati samo ako nema nikoga u sjeni.
Ruke napadaju prvu stvar koja uđe u njihov radijus percepcije, i ne popuštaju dok im meta nije mrtva.

Sjene neće izlaziti iz ruševina (bar ne danju), ali ako broj živih igrača u nekom trenutku bude manji od broja zombija, zombiji će krenuti prema Brandovcima koji čekaju vani. Ako ikoji Brandovac umre, to je -100XP-a kazne.

*Rezolucija:*
Branne moli igrače da dobro pregledaju prostoriju za bilo kakvim tragovima neprijatelja. Prilikom pretraživanja imaju priliku naći tajni prolaz za +50XP-a (DC15 Perception). Ako ga ne nađu, naći će ga Branne nakon par sati. U svakom slučaju, kada likovi potvrde da je mjesto sigurno, njena ekipa se baca na posao, istraživajući svaki pedalj prostorije. To je igračima prilika za odmor.

Nakon odmora Branne im dođe, držeči nekakav isklesani komad kamena u ruci, i otkriva im da je ruševina uistinu dwarfovskog porijekla, kao što je to ona predpostavila u prvoj ekspediciji. Kako nisu našli nikakve vrijedne stvari ovdje, Branne bi nastavila put niz tajni ulaz. Napominje da su se tajni ulazi radili poglavito za riznice.

*Potencijalne nagrade:* 280XP-a (70XP-a svakome) od neprijatelja
50XP-a (10 XP-a svakome) ako nađu tajni ulaz.
[:rel:top]
+++ Klopka u dubini
*Opis:*
Kameno stepenište niskog stropa vodi skupinu dublje u podzemlje ruševine. U ovim prostorima nema mnogo znakova prolaska vremena, i Branne gleda zadivljeno glatke kamene strukture pod svjetlom svoje baklje. Nakon nekog vremena, stepenice staju i skupina se nađe pred [Klopka sa strelicama|ulazom]. 
*OPCIONALNO*: druga klopka (ako je prošlo maksimalno 3/5 proteklog vremena).

*Potencijalne nagrade:* 50XP-a (12XP-a svakom) za svaku zamku koju su prošli bez ozljeda.

+++ Sklonište
*Opis:*
Ulaskom u prostoriju, očito je da to nije nikakva riznica. Prostorija je kvadratna, bez daljnjih izlaza. Četiri stupa drže strop od propadanja. Ostaci pokućstva se još donekle naziru. No pod prekrivaju posmrtni ostaci brojnih patuljaka, te nad njima stoje tri kostura, stojeći u stavu mirno. Na ulazak ekipe reagiraju nasilno, odmah ih napadajući.

*Neprijatelji:* 3 Skeleton(50XP) i 2 Warhorse Skeleton(100XP)

*Borba:* Manji skeletoni koriste svoja ranged oružja dok minotaur juriša po sobi. 
Ako su napadnuti izbliza, skeletoni ispuštaju svoje shortbow-ove i prime se mačeva, a lukove više ne koriste kasnije. 
Ako se likovi sakrivaju iza stupova, juriš minotaura će biti prekinut i minotaur će tresnuti o stup tako jako da mu se kosti zatresu. Ako je skeleton između minotaura i mete juriša, minotaur će prejahati preko kostura, radeći 1d4 + 1 bludgeoning damage (poduplaj zbog vulnerability).

*Rezolucija:*
Nakon borbe, ekipa pregledava što se sve može naći među starim kostima.
Od predmeta je gotovo sve uništeno vremenom, osim random art objekata, random magic itema, random love, zlatna kruna i bag of holding. U bag-u je samo jedna [Diary of Traril Stoneheart|knjiga], očuvana zahvaljujući magiji bag-a. Branne je tako oduševljena da im dopušta da zadrže sve osim knjige, krune i art objekata. Čak im poklanja i bag of holding, stavljajući knjigu u svoj. Knjiga je pisana na drevnom dwarfovskom. Kaže da će joj trebati neko vrijeme da dešifrira knjigu, ali ako ih zanima rezultat da ju posjete u prijestolnici za otprilike mjesec dana. 

*Potencijalne nagrade:* 350XP-a (88XP-a svakome)
Bag of Holding
Rollaj na [Random loot tables|tablici za nagrade] - treasure hoard 0-4, i ukloni sve dobivene art objekte (oni idu Branne).
Ostatci nagrade iz [Session 2|drugog session-a].
[:rel:top]

+++ Epilog
Branne i ekipa doputuju natrag do Taurel-a, usput slaveći uspješnu ekspediciju. Ona osobno otiđe u guild s likovima i nadgleda preuzimanje nagrade. Tamo još sve časti s pićem, zahvali im se, i otputuje na istok da dostavi umjetnine u muzej i počne sa proučavanjem pronađene knjige.

+++ Događaji
Vorko je morao izostati.
Ostatak ekipe je rasturio tamnicu (ali nisu obavili ništa opcionalno). Otkrili su tajna vrata za podrum. Toliko su izmučili stari mehanizam zamke da se soba kompletno uništila.
+++ Nagrade
Ukupno 680XP-a (170XP-a svakome)
Nešto love
7 dragulja od po 50g
Bag of Holding
Potion of Gaseous Form, Potion of Mind Reading, Oil of Etherealness
[:rel:top]
