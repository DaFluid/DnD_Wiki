﻿++ Jonov kamen

Jedan komad [Artefakti bogova|magičnog kamena] koji se našao u rukama Jona prilikom njegova odlaska iz hrama. Dan mu je na čuvanje od strane redovnika [Hram Otvoreni Dlan|hrama Otvoreni Dlan], kako bi na svojem putovanju možda saznao nešto više o njemu. Ima žućkastu boju.
Čini se kao da je sam [Vies] donio taj kamen u [Selo Viesa] jednom u prošlosti.

Kamen sadrži misteriozne magične moći, a promatrani efekti su bili:
    * razotkrivanje proročanstva o povratku bogova kod velikog kristala na granicia sa regijom [Seole] ([Session 2])
    * vizije kobnih trenutaka u budućnosti:
        *  vizije kako [Alias|Aliasa] i [Aadhan|Aadhana] pogađaju strijele ([Session 3])
        *  vizija napada na [Ostelath] (u snu u [Session 6|session-u 6], detalji otkriveni u [Session 7|session-u 7])
        *  vizija aktivacije brojnih klopki i [Allesandro|Allesandrove] pogibelji u [Session 10|session-u 10]
    * otkrivanje natpisa u tajnoj knjizi proroka [Varorin|Varorina] ([Session 4])
    * razotkrivanje iluzije nad kućom [Haldos|Haldosa] ([Session 4])
    * vizija korištenja magičnog papira iz [Session 9|9. session-a] (vizija pera koji piše po papiru)
    * snažan bljesak i sila koja odbije sve oko sebe kada se u kamen udari mačem ([Session 10])
