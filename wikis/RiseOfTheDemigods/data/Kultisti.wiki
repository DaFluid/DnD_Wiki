﻿++ Kultisti
[alias:Kult Viesa; Hudičari]

Skupina ljudi koji štuju poluboga [Vies|Viesa] i/ili [Mardoh|Mardoha]. Prepoznatljivi su po tamnim plaštevima sa kapuljačama (hudičari). Većinom su elfovi iz [Seole] regije, i ima ih u značajnom broju.
Cilj im je skupljati [Artefakti bogova|moćne artefakte] da bi se ojačali te propovjedima i širenjem straha (terorističkim napadima) povećali broj štovatelja.
