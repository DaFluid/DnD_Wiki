﻿++ Aadhanov kamen

Jedan od dva [Artefakti bogova|artefakta bogova] koje posjedujemo. Dan je [Aadhan|Aadhanu] na korištenje od strane [Haldos|Haldosa], nakon što je Aadhan "nabavio" jedan veći komad kamena iz magične tame u Haldosovom podrumu (vidi [Session 5]). Ima narančastu boju.
Puna moć kamena nam nije poznata, ali znamo da ima iduće efekte:
    * daje nam mogućnost komunikacije sa Haldosom (nije poznato je li to u potpunosti Haldosova magija ili i magija kamena igra tu neku ulogu)
        * ovaj efekt prestane raditi nakon događaja u [Session 6|šestom session-u], te nas umjesto toga transportira u neku ledenu dimenziju smrti
    * Aadhanu daje vizije prošlosti u ključnim trenutcima
        * [Session 5]: vizija [Kultisti|kultista] kako ulaze u špilju kroz tajni ulaz
        * [Session 9]: vizija osobe na tronu (vjerojatno [Kralj|kralj]) kako piše po magičnom papiru
        * [Session 10]: vizija kultista u [Selo Viesa|selu Viesa] kako prima [Jonov kamen] od nepoznate osobe (vjerojatno [Vies])