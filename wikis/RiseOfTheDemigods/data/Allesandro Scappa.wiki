﻿++ Allesandro Scappa
[alias:Allesandro]
Allesandro je mladi muškarac duge, plave kose, koji nosi istrošenu bijelu halju. Naizgled mirne, uljudne i pristojne naravi, proziva se putujućim propovjednikom i propovijeda mir i ljubav.
U stvarnosti Allesandro je prevarant koji je inspiraciju za svoju točku dobio od proroka po imenu [Varorin]. Vidjevši njegove propovijedi i kako utjeću na običan puk odlučio je preuzeti tu personu kako bi našao sebi neku korist.
Inače nije tip koji bi se izlagao opasnostima, ali njegova privrženost svojim prijateljima ga potiče da u ključnim trenutcima ipak iskaže malo hrabrosti.

S druge strane, za oko mu je zapela [Elfica|jedna kultistica], unatoč tome kao se ona odnosi prema njemu.


