﻿++ Session 8

*Datum održavanja: 15.12.2019.*

[Allesandro] se raspitao u gradu za [Aadhan|Aadhana] i o ljudima koji su oteli [Jon|Jona]. Na žalost nije ništa saznao, te na kraju samo unajmi konja i krene tragovima kočije.

Jon je proveo noć s [Alias|Aliasom] i njegovom ekipom. Nisu ga držali kao zarobljenika, i Alias mu je ispričao kako njegova skupina lovi [Kultisti|hudičare] te da žele uništiti sve [Artefakti bogova|magične kamene]. Jon im povjeruje, te pošto je Alias skužio da Jon ima [Jonov kamen|kamen] priznaje mu da ga je dobio u svom [Hram Otvoreni Dlan|hramu] i da mu daje vizije opasnosti. Jon pokušava sve da nagovori Aliasa da ga pusti s kamenom i da se rastanu kao saveznici, ali Alias odbija. Nudi mu da ili pođe s njima ili ostavi kamen kod njih i ode. Ne videći druge, Jon im pobjegne.
U taj tren Allesandro dojaše na konju. Iznenađeni Jon se popne na konja u trku i oni pobjegnu natrag prema [Ostelath|Ostealthu], otjerajući svoje proganjatelje sa strijelom upozorenja.

Aadhan, nakon što je bio zarobljen, biva otpraćen s dvojicom hudičara dok je [Elfica] otišla nekim drugim poslom. Svojim nekarakteristično vještim jezikom Aadhan uspije potplatiti jednoh hudičara sa 60g i svojom magičnom haljom te oni skupa ubiju drugog hudičara. Aadhan i izdajica otiđu van grada (Aadhan si kupi neki jeftini plašt), obave transakciju, te Aadhan od njega još sazna da je [Kralj|kralj] vođa kultista i da im je iduća meta [Valensias], glavni grad [Ezoras|Ezorasa]. Nakon toga ode u potragu za Allesandrom, i svi se sretnu na cesti koja vodi iz Ostelatha.

Nakon što svi podijele svoje priče, trojac se otputi u Valensias. Na putu si kupe drugu odjeću da se bar malo preruše. Pri dolasku u Valensias saznaju da je netko ubio gradolnačelnika i da je njegov zamjenik uhićen. Aadhan da Allesandru nevidljivost i on istraži vijećnicu, te vidi da je gradolnačelnik ustrijeljen crnom strijelom. Allesandru se čini da je [Elfica] imala istu. U jednom inn-u također saznaju da su hudičari viđeni u gradu i da su se otputili na istok. Jon još sazna za postojanje jednog hrama monkova 6 sati sjeverno od Valensiasa. Allesandro podijeli priču o Ostellathu sa ostalima u inn-u i krene raspravljati sa svojim kompanjonima kuda dalje.



[Session 7|< Prethodni session] | [Session 9|Idući session >]