[wiki_db]
data_dir = data

[main]
wikiwidehistory_maxentries = 100
footnotes_as_wikiwords = False
versioning_storagelocation = 0
last_wiki_word = Session 12
hotkey_showhide_bywiki = 
log_window_autoshow = Gray
filestorage_identity_filenamemustmatch = False
trashcan_storagelocation = 0
export_default_dir = 
trashcan_maxnoofbags = 200
tabs_maxcharacters = 0
trashcan_askondelete = True
wiki_icon = 
headingsasaliases_depth = 0
wiki_name = RiseOfTheDemigods
wikipagetitle_creationmode = 1
wikipagetitleprefix = ++
wikipagetitle_fromlinktitle = False
wiki_lastactivetabno = 0
first_wiki_word = 
wikipagefiles_gracefuloutsideaddandremove = True
indexsearch_enabled = False
wiki_lasttabssubctrls = textedit;textedit;textedit;textedit;preview;textedit;textedit;textedit;textedit;textedit
template_pagenamesre = ^template/
tree_expandednodes_rememberduration = 2
tabhistory_maxentries = 25
tree_expandednodes_descriptorpathes_main = wikipage/RiseOfTheDemigods,wikipage/Session 8;wikipage/RiseOfTheDemigods,wikipage/Session 1;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar,wikipage/Artefakti bogova;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7,wikipage/Session 6,wikipage/Session 5,wikipage/Session 4,wikipage/Session 3,wikipage/Session 2;wikipage/RiseOfTheDemigods,wikipage/Jon Hunter,wikipage/Jonov kamen;wikipage/RiseOfTheDemigods,wikipage/Session 1,wikipage/Ostelath;wikipage/RiseOfTheDemigods,wikipage/Jon Hunter,wikipage/Jonov kamen,wikipage/Session 10,wikipage/Session 9,wikipage/Session 8;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar,wikipage/Artefakti bogova,wikipage/Jonov kamen,wikipage/Session 10;wikipage/RiseOfTheDemigods,wikipage/Session 9,wikipage/Session 8;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7,wikipage/Session 6,wikipage/Session 5,wikipage/Session 4,wikipage/Session 3;wikipage/RiseOfTheDemigods,wikipage/Session 9,wikipage/Seole;wikipage/RiseOfTheDemigods,wikipage/Session 6,wikipage/Aadhanov kamen;wikipage/RiseOfTheDemigods,wikipage/Kontinent,wikipage/Ezoras;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Simboli;wikipage/RiseOfTheDemigods,wikipage/Aadhan;wikipage/RiseOfTheDemigods,wikipage/Session 7;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar,wikipage/Artefakti bogova,wikipage/Jonov kamen,wikipage/Session 10,wikipage/Session 11;wikipage/RiseOfTheDemigods,wikipage/Session 11;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Jon;wikipage/RiseOfTheDemigods,wikipage/Session 3;wikipage/RiseOfTheDemigods,helpernode/main/view;wikipage/RiseOfTheDemigods,wikipage/Session 1,wikipage/Awistan;wikipage/RiseOfTheDemigods,wikipage/Jon Hunter,wikipage/Jonov kamen,wikipage/Session 10,wikipage/Session 9;wikipage/RiseOfTheDemigods,wikipage/Kontinent,wikipage/Awistan;wikipage/RiseOfTheDemigods,wikipage/Kralj;wikipage/RiseOfTheDemigods,wikipage/Session 3,wikipage/Session 4;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7,wikipage/Session 6,wikipage/Session 5,wikipage/Session 4;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar,wikipage/Artefakti bogova,wikipage/Jonov kamen,wikipage/Artefakti bogova;wikipage/RiseOfTheDemigods,wikipage/Kontinent,wikipage/Ezoras,wikipage/Alias;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7,wikipage/Session 6;wikipage/RiseOfTheDemigods,wikipage/Session 10;wikipage/RiseOfTheDemigods,wikipage/Session 9,wikipage/Selo Viesa;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9;wikipage/RiseOfTheDemigods,wikipage/Session 3,wikipage/Malagar;wikipage/RiseOfTheDemigods,wikipage/Session 2;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar,wikipage/Artefakti bogova,wikipage/Jonov kamen;wikipage/RiseOfTheDemigods,wikipage/Session 6,wikipage/Dnevnik kultista;wikipage/RiseOfTheDemigods;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9;wikipage/RiseOfTheDemigods,wikipage/Kontinent;wikipage/RiseOfTheDemigods,wikipage/Session 9;wikipage/RiseOfTheDemigods,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Allesandro;wikipage/RiseOfTheDemigods,wikipage/Session 2,wikipage/Malagar;wikipage/RiseOfTheDemigods,wikipage/Session 4,wikipage/General Hagar;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7;wikipage/RiseOfTheDemigods,wikipage/Session 5;wikipage/RiseOfTheDemigods,wikipage/Session 7,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 9,wikipage/Session 8,wikipage/Session 7,wikipage/Session 6,wikipage/Session 5;wikipage/RiseOfTheDemigods,wikipage/Jon Hunter,wikipage/Jonov kamen,wikipage/Session 10;wikipage/RiseOfTheDemigods,wikipage/Session 1,wikipage/Ostelath,wikipage/Henou;wikipage/RiseOfTheDemigods,helpernode/main/view,helpernode/main/parentless;wikipage/RiseOfTheDemigods,wikipage/Session 10,wikipage/Session 11
wiki_wikilanguage = wikidpad_default_2_0
tree_force_scratchpad_visibility = True
further_wiki_words = Mag\x20Valensiasa;Simboli;Session\x202;Kontinent;Elfica;Artefakti\x20bogova;Session\x2010;RiseOfTheDemigods;Jon\x20Hunter
tree_expandednodes_descriptorpathes_views = 
tree_last_root_wiki_word = RiseOfTheDemigods
db_pagefile_suffix = .wiki
filestorage_identity_moddatemustmatch = False
filestorage_identity_moddateisenough = False
wiki_database_type = original_sqlite
wikipagefiles_asciionly = True
indexsearch_formatno = 1
versioning_completesteps = 10
wiki_readonly = False

