Import-Module PSSQLite

$Database = "../wikis/RiseOfTheDemigods/data/wikiovw.sli"
$BasePath = "../wikis/RiseOfTheDemigods/data/"
[regex]$Matcher = '\r\n------------------------'

#SQLite will create Names.SQLite for us
$Wikiwords = Invoke-SqliteQuery -Query "SELECT word, filepath FROM wikiwords WHERE word <> 'RiseOfTheDemigods' AND word <> 'ScratchPad'" -DataSource $Database

foreach ($word in $Wikiwords){

    $filepath = $BasePath + $word.filepath
    if(!(Test-Path $filepath)){
        continue
    }

    $wiki = Get-Content -path $filepath -Raw -Encoding UTF8
    $referenceLine = $wiki.IndexOf("page(s) referring to")
    if($referenceLine -gt -1) {
        $lines = $Matcher.Matches($wiki)
        $lines = $lines.Where({$_.Index -lt $referenceLine}) | Sort-Object -Property Index
        $topLine = $lines[$lines.Count-1].Index
        $wiki = $wiki.Substring(0, $topLine);
        Set-Content -path $filepath -Value $wiki -Encoding UTF8
    }
}