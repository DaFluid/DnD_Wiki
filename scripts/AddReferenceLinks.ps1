#Procedura: Prvo pokreni RemoveReferenceLinks. Onda pokreni rebuild wikija (probaj Maintenance - Initiate Update prvo, ak ne uspije pokreni rebuild pa ponovno update).
#           Kad si siguran da su sve relacije ciste, pokreni ovu skriptu.
Import-Module PSSQLite

$Database = "../wikis/RiseOfTheDemigods/data/wikiovw.sli"
$BasePath = "../wikis/RiseOfTheDemigods/data/"
[regex]$Matcher = '\r\n------------------------'

#SQLite will create Names.SQLite for us
$Wikiwords = Invoke-SqliteQuery -Query "SELECT word, filepath FROM wikiwords WHERE word <> 'RiseOfTheDemigods' AND word <> 'ScratchPad'" -DataSource $Database

foreach ($word in $Wikiwords){
    $changeMade = $false

    $filepath = $BasePath + $word.filepath
    if(!(Test-Path $filepath)){
        continue
    }

    $wiki = Get-Content -path $filepath -Raw -Encoding UTF8
    $referenceLine = $wiki.IndexOf("page(s) referring to")
    if($referenceLine -gt -1) {
        $lines = $Matcher.Matches($wiki)
        $lines = $lines.Where({$_.Index -lt $referenceLine}) | Sort-Object -Property Index
        $topLine = $lines[$lines.Count-1].Index
        $wiki = $wiki.Substring(0, $topLine);
        $changeMade = $true
    }

    $query = "SELECT word FROM wikirelations WHERE relation = '"+$word.word + "' AND word <> '"+$word.word + "'"
    $linksTo = @(Invoke-SqliteQuery -Query $query -DataSource $Database)

    if($linksTo.Length -gt 0) {
        $text = "`r`n------------------------`r`n"
        $text += "*"+$linksTo.Length+" page(s) referring to* [//"+$word.word+"]`r`n"
        foreach($link in $linksTo){
            $text += "[//"+ $link.word+"]`r`n"
        }
        $text += "------------------------"

        $wiki += $text
        $changeMade = $true
    }

    if($changeMade){
        Set-Content -path $filepath -Value $wiki -Encoding UTF8
    }
}